import dotenv from 'dotenv';

dotenv.config();

const port = process.env.SERVER_PORT || '';
const url = process.env.MANGO_URL || '';

const serverPort = port ? Number(port) : 80;

const apiKey = process.env.GOOGLE_MAPS_API_KEY;

export const config = {
    mongo : {
        url : url
    },
    server : {
        port : serverPort
    },
    geocodeApi : {
        key : apiKey
    }
}